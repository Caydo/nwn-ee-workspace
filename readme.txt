/////////////////////////////////////////////////////
//
// NWN: EE (Neverwinter Nights: Enhanced Edition) Workspace
//
// Features Included:
// * Intellisense
// * Compilation of single file
// * Compilatiton of all files
// * Auto launch of a module for a player from a keyboard shortcut.
// * Auto launch the DM client from a keyboard shortcut.
//
// This workspace was set up using the tutorial located at: https://nwnlexicon.com/index.php?title=NWN:EE_Script_Editing_Tutorial
// You will need:
// * VSCode (https://code.visualstudio.com/) - To edit/compile code, run tasks, etc.
// * Git Bash For Windows (https://gitforwindows.org/) - To run the automation.
// * NWN: EE installed via Steam (https://store.steampowered.com/app/704450/Neverwinter_Nights_Enhanced_Edition/) or GOG
//
// Built out by Caydo (Joseph Smits)
/////////////////////////////////////////////////////

Set up for automation for auto load of module/DM client:
1. Open automationValues.txt.2
2. Update the path of gameDirectory to your steam install. Any directory name with a space needs to be surrounded by ' characters. It is recommended to keep the path all lowercase.
3. Update the moduleName to whatever module you want to auto load.

To get your workspace set up with your local installation locations:
1. Install VSCode.
2. Install Git Bash for Windows.
3. Install NWN: EE.

To set up compilation tasks:
1. Open VSCode.
2. Select File then Open Workspace From File and select nwnee.code-workspace.
3. Select tasks.json under nwn-ee-workspace -> .vscode. (this file can be opened in a text editor like notepad++ as well for editing)
4. In the section with the label "BuildScript" edit the two file paths in the args section to point to your NWN: EE installs for the temp0 folder and nwscript.nss file.
 * If you have installed NWN: EE to its default location on your C drive, then you need only change "YOURUSERNAME" to whatever your Windows username is.
 
To set up keyboard shortcuts to run the tasks:
1. Open VSCode.
2. Select File then Open Workspace From File and select nwnee.code-workspace.
3. Select File then Preferences then Keyboard Shortcuts.
4. At the top right of the keyboard shortcuts window select "Open Keyboard Shortcuts JSON".
5. Paste the contents of keybindings.json in there.

Alternative to pasting keybindings.json contents:
1. Navigate to the Roaming folder for VSCode in your user app data (by default this will be C:\Users\YOURUSERNAME\AppData\Roaming\Code\User)
2. Replace keybindings.json with the keybindings.json file included in the repo.

Sync up NWN scripts in an open module with VSCode:
NOTE THAT THE TEMP0 FOLDER WILL ONLY BE VISIBLE IN VSCODE WHILE THE AURORA TOOLSET IS OPEN (and will be visible/not visible as you open/close the toolset)
1. Open VSCode.
2. Select File then Open Workspace From File and select nwnee.code-workspace.
3. Open NWN: EE Aurora Toolset (default install location is C:\Steam\steamapps\common\Neverwinter Nights\bin\win32\nwtoolset.exe or run it from Steam).
4. Open a module or create a new one.
5. Return to VSCode then select File then Add Folder To Workspace.
6. Navigate to the temp0 folder location where scripts for the open module are unpacked and select it. (default is C:\Users\YOURUSERNAME\Documents\Neverwinter Nights\modules)

To compile a single file:
1. Open VSCode.
2. Select File then Open Workspace From File and select nwnee.code-workspace.
3. Open NWN: EE Aurora Toolset (default install location is C:\Steam\steamapps\common\Neverwinter Nights\bin\win32\nwtoolset.exe or run it from Steam).
4. Open a module or create a new one.
5. Return to VSCode and open any of the script files (.nss).
6. With an nss file open press F6 (or whatever keyboard shortcut you set in the keyboard shortcut setup section.)

To compile all code for an open module:
1. Open VSCode.
2. Select File then Open Workspace From File and select nwnee.code-workspace.
3. Open NWN: EE Aurora Toolset (default install location is C:\Steam\steamapps\common\Neverwinter Nights\bin\win32\nwtoolset.exe or run it from Steam).
4. Open a module or create a new one.
5. Return to VSCode and open any of the script files (.nss).
6. With an nss file open press F8 (or whatever keyboard shortcut you set in the keyboard shortcut setup section.)

* Your code changes should have effect immediately if you change a script in VSCode, save it (or have auto save on), compile, then open the script in the Aurora toolset.

* In order to see your script changes at runtime when testing your module, you will need to make a change in the toolset then save the module. My usual approach to this has been a slight movement of an NPC then putting them back. For example:
1. Select the Paint Items button then select an NPC in an area.
2. Hold CTRL then scroll wheel up then down. This causes the toolset to see a change in the position of the NPC (though you only moved them then moved them back.)
3. Save your module and you should now see code changes at runtime when running the module.